import uuid as uuid

from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.utils import timezone


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, first_name, last_name, birth_date, password, username=None, email=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        extra_fields.setdefault("is_active", True)

        if not first_name:
            raise ValueError("first_name is required.")
        if not last_name:
            raise ValueError("last_name is required.")
        if not birth_date:
            raise ValueError("birth_date is required.")
        if not password:
            raise ValueError("password is required.")
        if not username:
            username = f'{first_name} {last_name} {birth_date}'

        email = self.normalize_email(email)
        user = self.model(first_name=first_name, last_name=last_name, birth_date=birth_date,
                          username=username, email=email,  **extra_fields)
        user.set_password(password)
        user.save()

    def create_superuser(self, first_name, last_name, birth_date, password, username, email, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)

        if not first_name:
            raise ValueError("first_name is required.")
        if not last_name:
            raise ValueError("last_name is required.")
        if not birth_date:
            raise ValueError("birth_date is required.")
        if not password:
            raise ValueError("password is required.")
        if not username:
            raise ValueError("username is required.")
        if not email:
            raise ValueError("email is required.")

        email = self.normalize_email(email)
        user = self.model(first_name=first_name, last_name=last_name, birth_date=birth_date,
                          username=username, email=email,  **extra_fields)
        user.set_password(password)
        user.save()


class ExtendedUser(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    """Define model for our extended user without username field"""
    username = models.CharField("username", max_length=32, null=False, blank=False, unique=True,
                                validators=(username_validator,))
    email = models.EmailField("email address", unique=True, null=True, blank=True)
    first_name = models.CharField("first name", max_length=32, null=False, blank=False)
    last_name = models.CharField("last name", max_length=32, null=False, blank=False)
    birth_date = models.DateField("birth date", blank=False, null=False)
    uuid = models.UUIDField("uuid", unique=True, default=uuid.uuid4, editable=False, db_index=True)
    is_staff = models.BooleanField("is staff", default=False, null=False, blank=True)
    is_active = models.BooleanField("is active", default=True, null=False, blank=True)
    date_joined = models.DateTimeField("date joined", default=timezone.now, null=False, blank=True)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'birth_date', "email"]
    objects = UserManager()

    def str(self):
        return f'ExtendedUser(first_name={self.first_name}, last_name={self.last_name}, birth_date={self.birth_date})'

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"
        unique_together = ('first_name', 'last_name', 'birth_date')
        db_table = 'user'
