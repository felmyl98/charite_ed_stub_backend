from django.contrib import admin
from django.contrib.auth import get_user_model


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'id', 'birth_date', 'uuid')
    readonly_fields = ('id', 'uuid')


admin.site.register(get_user_model(), UserAdmin)
