from django.contrib import admin
from patients.models import Patient


class PatientAdmin(admin.ModelAdmin):
    list_display = ('user_id',)
    readonly_fields = ('user_id',)


admin.site.register(Patient, PatientAdmin)
