from django.contrib import admin

from doctors.models import Doctor


class DoctorAdmin(admin.ModelAdmin):
    list_display = ('user_id',)
    readonly_fields = ('user_id',)


admin.site.register(Doctor, DoctorAdmin)
