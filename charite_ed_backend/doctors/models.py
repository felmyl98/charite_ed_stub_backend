from django.db import models
from django.contrib.auth import get_user_model


class Doctor(models.Model):
    user = models.OneToOneField(to=get_user_model(), null=False, blank=False, on_delete=models.CASCADE,
                                primary_key=True)
