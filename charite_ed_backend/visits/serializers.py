from visits.models import HospitalVisit, Instruction
from rest_framework import serializers


class HospitalVisitSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(source='department.name')

    class Meta:
        model = HospitalVisit
        fields = ('uuid', 'department_name', 'date', 'purpose')


class InstructionSerializer(serializers.ModelSerializer):
    doctor_name = serializers.CharField(source='doctor.user.last_name')
    ed_visit = serializers.CharField(source="visit.uuid")

    class Meta:
        model = Instruction
        fields = ('uuid', 'ed_visit', 'doctor_name', 'instructions', 'diagnosis')


class GuidanceSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(source='department.name')
    instructions = InstructionSerializer(source='instruction_set', many=True)

    class Meta:
        model = HospitalVisit
        fields = ('uuid', 'department_name', 'date', 'purpose', 'instructions')
