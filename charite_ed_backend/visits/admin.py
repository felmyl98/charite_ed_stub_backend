from django.contrib import admin
from django.contrib.auth import get_user_model
from visits.models import Department, Instruction, HospitalVisit


class HospitalVisitAdmin(admin.ModelAdmin):
    list_display = ('patient', 'department', 'date', 'purpose')
    readonly_fields = ('id', 'uuid')


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'street', 'house_number', 'postcode')


class InstructionAdmin(admin.ModelAdmin):
    list_display = ('visit', 'doctor', 'instructions', 'diagnosis')
    readonly_fields = ('id', 'uuid')


admin.site.register(HospitalVisit, HospitalVisitAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Instruction, InstructionAdmin)
