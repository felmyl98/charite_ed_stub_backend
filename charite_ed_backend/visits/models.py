import uuid as uuid
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from doctors.models import Doctor
from patients.models import Patient


class Department(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64, null=False, blank=False)
    street = models.CharField(max_length=64, null=False, blank=False, unique=False)
    house_number = models.CharField(max_length=6, null=False, blank=False, unique=False)
    postcode = models.PositiveIntegerField(null=False, blank=False, validators=(MinValueValidator(limit_value=0),
                                                                                MaxValueValidator(limit_value=99999)))

    class Meta:
        verbose_name = "ED Department"
        verbose_name_plural = "ED Departments"
        db_table = "department"

    def __str__(self):
        return self.name


class HospitalVisit(models.Model):
    id = models.IntegerField(primary_key=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, db_index=True)
    patient = models.ForeignKey(to=Patient, on_delete=models.CASCADE, null=False, blank=False)
    department = models.ForeignKey(to=Department, on_delete=models.PROTECT, null=False, blank=False)
    date = models.DateField(null=False, blank=False, default=None, unique=False)
    purpose = models.CharField(max_length=64, null=False, blank=False)
    # document = ContentTypeRestrictedFileField(
    #     upload_to="uploads/%Y/%m/%d/",
    #     content_types=[
    #         "application/pdf"
    #     ],
    #     max_upload_size=5242880,
    #     blank=False,
    #     null=False,
    # )

    class Meta:
        verbose_name = "Hospital Visit"
        verbose_name_plural = "Hospital Visits"
        db_table = "hospital_visit"

    def __str__(self):
        return str(self.uuid)


class Instruction(models.Model):
    id = models.IntegerField(primary_key=True)
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, db_index=True)
    visit = models.ForeignKey(to=HospitalVisit, on_delete=models.CASCADE, null=False, blank=False)
    doctor = models.ForeignKey(to=Doctor, on_delete=models.PROTECT, null=False, blank=False)
    instructions = models.TextField(null=False, blank=True, default="", unique=False)
    diagnosis = models.CharField(max_length=64, null=False, blank=True, default="", unique=False)

    class Meta:
        verbose_name = "Instruction"
        verbose_name_plural = "Instructions"
        db_table = "instruction"

    def __str__(self):
        return str(self.uuid)
