import mimetypes
import os
from wsgiref.util import FileWrapper

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from django.http import HttpResponse, HttpResponseNotFound
from .models import Instruction, HospitalVisit
from .serializers import InstructionSerializer, HospitalVisitSerializer, GuidanceSerializer


class HospitalVisitApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        visits = HospitalVisit.objects.filter(patient=request.user.id)
        serializer = GuidanceSerializer(visits, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = {
            'task': request.data.get('task'),
            'completed': request.data.get('completed'),
            'user': request.user.id
        }
        serializer = HospitalVisitSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class GuidancesView(APIView):
#     permission_classes = [permissions.IsAuthenticated]
#
#     def get(self, request, *args, **kwargs):
#         visits = HospitalVisit.objects.filter(patient=request.user.id)
#         serializer = HospitalVisitSerializer(visits, many=True)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     def post(self, request, *args, **kwargs):
#         data = {
#             'task': request.data.get('task'),
#             'completed': request.data.get('completed'),
#             'user': request.user.id
#         }
#         serializer = HospitalVisitSerializer(data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InstructionsApiView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, visit_id, *args, **kwargs):
        visits = Instruction.objects.filter(visit=visit_id)
        serializer = InstructionSerializer(visits, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        data = {
            'task': request.data.get('task'),
            'completed': request.data.get('completed'),
            'user': request.user.id
        }
        serializer = InstructionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def DownloadPdf(request):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    filename = 'example.pdf'
    file_path = BASE_DIR + '/visits/' + filename
    print(file_path)
    if os.path.exists(file_path):
            f = open(file_path, "rb")
            wrapper = FileWrapper(f)
            response = HttpResponse(wrapper, content_type='application/pdf')

            response['Content-Length'] = os.path.getsize(file_path)
            response['Content-Disposition'] = "attachment; filename=" + filename

            return response
    return HttpResponseNotFound
